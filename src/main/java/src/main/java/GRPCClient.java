package src.main.java;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import src.main.java.User;
import src.main.java.userGrpc;
import src.main.java.userGrpc.userBlockingStub;

public class GRPCClient {
    public static void main(String[] args){
        ManagedChannel channel= ManagedChannelBuilder.forAddress("localhost",9090).usePlaintext().build();
        userBlockingStub userStub = userGrpc.newBlockingStub(channel);
        User.LoginRequest loginRequest= User.LoginRequest.newBuilder().setUsername("Ram").setPassword("Ram").build();
        User.APIResponse response= userStub.login(loginRequest);
        System.out.println("Medication: paracetamol, Doza:2");
        System.out.println(response.getResponsemessage());
    }
}
