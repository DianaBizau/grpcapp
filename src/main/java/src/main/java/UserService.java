package src.main.java;

import io.grpc.stub.StreamObserver;
import src.main.java.User.LoginRequest;
import src.main.java.User.APIResponse;
public class UserService extends userGrpc.userImplBase {
    @Override
    public void login(LoginRequest request, StreamObserver<APIResponse> responseObserver) {


        System.out.println("Inside");
        String username=request.getUsername();
        String password=request.getPassword();

        APIResponse.Builder response= APIResponse.newBuilder();
        if(username.equals(password)){
            response.setResponseCode(0).setResponsemessage("SUCCESS");
        }
        else{
            response.setResponseCode(100).setResponsemessage("INVALID PASSWORD");
        }
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();

    }

    @Override
    public void logout(User.Empty request, StreamObserver<User.APIResponse> responseObserver) {

    }
}
